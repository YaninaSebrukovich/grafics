document.addEventListener('DOMContentLoaded', () => { // структура документа загружена   
    new Chart( // инициализируем плагин
        document.querySelector('.chart'), // первым параметром передаем элемент canvas по селектору
        // вторым параметром передаем настройки в виде объекта
        { 
            type: 'line', // тип графика, в данном случае линейный
            // type: 'bar', // изменили тип графика,
            data: { // общие данные графика в виде объекта
                labels: ['April', 'May', 'June', 'July', 'August'], // метки по оси X
                datasets: [ // набор данных, который будет отрисовываться в виде массива с объектами
                    { 
                        label: 'Books read', // название для определенного графика в виде строки
                        data: [3, 6, 2, 7, 4], // данные в виде массива с числами, количество должно совпадать с количеством меток по оси X
                        borderColor: 'crimson', // назначаем цвет для линий в виде строки
                        borderWidth: 5, // назначаем ширину линий
                        cubicInterpolationMode: 'monotone', // добавили сглаживание углов
                        backgroundColor: 'crimson', // назначаем столбцам фон
                        // fill: true // залили линейный график цветом
                    },

                    // добавили еще один график с другими значениями и цветом
                    {
                        label: 'Books bought',
                        data: [5, 2, 3, 1, 4],
                        borderColor: 'teal',
                        borderWidth: 5,
                        backgroundColor: 'teal',
                        cubicInterpolationMode: 'monotone'
                    },

                       // добавили еще один график с другими значениями и цветом
                    {
                        label: 'Books bought',
                        data: [1, 5, 2, 6, 3],
                        borderColor: '#36A2EB',
                        borderWidth: 5,
                        backgroundColor: '#36A2EB',
                        cubicInterpolationMode: 'monotone'
                    }
                ]
            },
            options: { // дополнительные опции для графика в виде объекта, если не нужны - передаем пустой объект
                scales: {
                    y: {
                        beginAtZero: true // назначили оси Y начинать отсчет с нуля
                    }
                }
            }
        }
    );
})
